var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", "BNO055_8py" ],
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", "DRV8847_8py" ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "LAB0.py", "LAB0_8py.html", null ],
    [ "LAB_01.py", "LAB__01_8py.html", "LAB__01_8py" ],
    [ "main.py", "main_8py.html", null ],
    [ "mainTP.py", "mainTP_8py.html", null ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", [
      [ "task_controller.Controller", "classtask__controller_1_1Controller.html", "classtask__controller_1_1Controller" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_encoder", "classtask__encoder_1_1Task__encoder.html", "classtask__encoder_1_1Task__encoder" ]
    ] ],
    [ "task_IMU.py", "task__IMU_8py.html", [
      [ "task_IMU.Task_imu", "classtask__IMU_1_1Task__imu.html", "classtask__IMU_1_1Task__imu" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.MotorTasks", "classtask__motor_1_1MotorTasks.html", "classtask__motor_1_1MotorTasks" ]
    ] ],
    [ "task_safety.py", "task__safety_8py.html", [
      [ "task_safety.SafetyTask", "classtask__safety_1_1SafetyTask.html", "classtask__safety_1_1SafetyTask" ]
    ] ],
    [ "task_touch.py", "task__touch_8py.html", [
      [ "task_touch.Task_touch", "classtask__touch_1_1Task__touch.html", "classtask__touch_1_1Task__touch" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", [
      [ "task_user.Task_user", "classtask__user_1_1Task__user.html", "classtask__user_1_1Task__user" ]
    ] ],
    [ "touch_panel.py", "touch__panel_8py.html", "touch__panel_8py" ]
];
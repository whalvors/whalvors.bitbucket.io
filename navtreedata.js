/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Will Halvorsen Mechatronics Portfolio", "index.html", [
    [ "Mechatronics Portfolio", "index.html", [
      [ "Introduction", "index.html#intro", null ]
    ] ],
    [ "Homework 0x00", "HW_0x00.html", [
      [ "Elevator State Transition Diagram", "HW_0x00.html#sec", null ]
    ] ],
    [ "Homework 0x02", "HW_0x02.html", [
      [ "CAD Rendering of Pivoting Platform", "HW_0x02.html#CAD", null ],
      [ "Dimensions of Parts to be Used in Analysis", "HW_0x02.html#Dims", null ],
      [ "Analysis of Ball Balancer System", "HW_0x02.html#Analysis", [
        [ "Simplified Schematic of Linkage OADQ", "HW_0x02.html#OADQ", null ],
        [ "Ball Motion Kinematics", "HW_0x02.html#Ball", null ],
        [ "Effective Moment from Motor Torque", "HW_0x02.html#Tm", null ],
        [ "Kinetic Analysis of the Platform and Ball System", "HW_0x02.html#KD1", null ],
        [ "Kinetic Analysis of the Ball", "HW_0x02.html#KD2", null ],
        [ "Matrix Manipulated Result", "HW_0x02.html#Mf", null ]
      ] ],
      [ "Matlab code for the matrix manupulation above", "HW_0x02.html#model", null ]
    ] ],
    [ "Homework 0x03", "HW_0x03.html", [
      [ "Linearize the system model developed in HW 0x02", "HW_0x03.html#linear", null ],
      [ "Open-loop model for two various cases", "HW_0x03.html#open", [
        [ "Case 1:The ball is initially at rest on a level platform directly above the center of gravity of the platform and there is no torque input from the motor", "HW_0x03.html#case1", null ],
        [ "Case 2:The ball is initially at rest on a level platform offset horizontally from the center of gravity of the platform by 5cm and there is no torque input from the motor", "HW_0x03.html#case2", null ],
        [ "Matlab code for the open-loop simulation", "HW_0x03.html#openloop", null ]
      ] ],
      [ "Closed-loop model for the offset case", "HW_0x03.html#closed", [
        [ "Matlab code for the closed-loop simulation", "HW_0x03.html#closedloop", null ]
      ] ],
      [ "This simulation can also be completed by designing a controller for the system in simulink", "HW_0x03.html#control", [
        [ "Matlab code for the controller", "HW_0x03.html#controlcode", null ]
      ] ]
    ] ],
    [ "Lab 0x00 Fibonacci Calculator", "Lab_0x00.html", [
      [ "Source Code", "Lab_0x00.html#sc00", null ],
      [ "Fibonacci Calculator Documentation", "Lab_0x00.html#dc00", null ]
    ] ],
    [ "Lab 0x01 Getting Started with Hardware", "Lab_0x01.html", [
      [ "State Diagram for Lab 0x01", "Lab_0x01.html#img1", null ],
      [ "Video Demonstration for Lab0x01", "Lab_0x01.html#vid1", null ],
      [ "Source Code", "Lab_0x01.html#sc01", null ],
      [ "Lab 0x01 Documented Code", "Lab_0x01.html#dc01", null ]
    ] ],
    [ "Lab 0x02 Incremental Encoders", "Lab_0x02.html", [
      [ "State Transition Diagram for Lab 0x02", "Lab_0x02.html#img2", null ],
      [ "Source Code", "Lab_0x02.html#sc02", null ],
      [ "Lab 0x02 Documented Code", "Lab_0x02.html#dc02", null ]
    ] ],
    [ "Lab 0x03 PMDC Motors", "Lab_0x03.html", [
      [ "State Transition Diagram for Lab 0x03", "Lab_0x03.html#img3", null ],
      [ "Data Plots", "Lab_0x03.html#results3", null ],
      [ "Source Code", "Lab_0x03.html#sc03", null ],
      [ "Lab 0x03 Documented Code", "Lab_0x03.html#dc03", null ]
    ] ],
    [ "Lab 0x04 Closed Loop Speed Control", "Lab_0x04.html", [
      [ "State Transition Diagram for Lab 0x03", "Lab_0x04.html#img4", null ],
      [ "Block Diagram for Closed Loop Controller", "Lab_0x04.html#img4block", null ],
      [ "Lab 0x04 Documented Code", "Lab_0x04.html#dc04", null ]
    ] ],
    [ "Lab 0x05 I2C and Inertial Measurement Units", "Lab_0x05.html", [
      [ "Video Demonstration for Lab0x05", "Lab_0x05.html#vid5", null ],
      [ "Source Code", "Lab_0x05.html#sc05", null ]
    ] ],
    [ "Term Project Ball Balancer", "TP.html", [
      [ "Ball Platform Modeling", "TP.html#Model", [
        [ "Reading from Touch Panel", "TP.html#touch", null ],
        [ "Balancing Ball", "TP.html#ball", null ],
        [ "Term Project Task Diagram", "TP.html#TPTASK", null ],
        [ "Term Project Results Video", "TP.html#TPVID", null ]
      ] ],
      [ "Term Project Documented Code", "TP.html#dcTP", null ],
      [ "Term Project Source Code", "TP.html#scTP", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BNO055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
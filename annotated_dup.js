var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "closedloop", null, [
      [ "ClosedLoop", "classclosedloop_1_1ClosedLoop.html", "classclosedloop_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_controller", null, [
      [ "Controller", "classtask__controller_1_1Controller.html", "classtask__controller_1_1Controller" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_encoder", "classtask__encoder_1_1Task__encoder.html", "classtask__encoder_1_1Task__encoder" ]
    ] ],
    [ "task_IMU", null, [
      [ "Task_imu", "classtask__IMU_1_1Task__imu.html", "classtask__IMU_1_1Task__imu" ]
    ] ],
    [ "task_motor", null, [
      [ "MotorTasks", "classtask__motor_1_1MotorTasks.html", "classtask__motor_1_1MotorTasks" ]
    ] ],
    [ "task_safety", null, [
      [ "SafetyTask", "classtask__safety_1_1SafetyTask.html", "classtask__safety_1_1SafetyTask" ]
    ] ],
    [ "task_touch", null, [
      [ "Task_touch", "classtask__touch_1_1Task__touch.html", "classtask__touch_1_1Task__touch" ]
    ] ],
    [ "task_user", null, [
      [ "Task_user", "classtask__user_1_1Task__user.html", "classtask__user_1_1Task__user" ]
    ] ],
    [ "touch_panel", null, [
      [ "Touch_Panel", "classtouch__panel_1_1Touch__Panel.html", "classtouch__panel_1_1Touch__Panel" ]
    ] ]
];